基于node环境爬取[DataV.GeoAtlas地理小工具系列 (aliyun.com)](https://datav.aliyun.com/portal/school/atlas/area_selector)的geoJSON中国地理行政范围的数据



#### 使用说明

1. 安装依赖

```
npm install
```

2. 获取地址数据

```
node get.js
```



爬虫执行完毕会生成data目录，里面都是geo数据





