const axios = require('axios');
const fs = require("fs");
const colors = require('colors');






function getData(code = "100000_full") {
  console.log(code);
  return new Promise((resolve, reject) => {
    axios.get("https://geo.datav.aliyun.com/areas_v3/bound/" + code + ".json").then(async (res) => {
      console.log('============⬇ 读取成功 ⬇============\n'.yellow);
      console.log(res.data);
      const writerStream = await fs.createWriteStream("data/" + parseInt(code) + '.json');
      await writerStream.write(JSON.stringify(res.data));
      await writerStream.end();
      console.log('============⬆ 写入成功 ⬆============\n'.green);
      resolve(res.data);

      // 下钻子项
      if (res.data.features.length > 1) {
        for await (const item of res.data.features) {
          console.log(item);
          if (item.properties.childrenNum) {
            await getData(item.properties.adcode + "_full")
          } else {
            await getData(item.properties.adcode)
          }
        }
      }
    }).catch(err => {
      getData(code)
      console.log('========== 数据读取错误 =========='.red);
      // reject("数据读取错误")
    })
  })
};
getData();

